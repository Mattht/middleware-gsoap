/* 
 * File:   Bank.cpp
 * Author: mattht
 * 
 * Created on 14 November 2012, 11:40
 */

#include "Bank.h"

Bank::Bank() 
{    
    this->id = 0;
    //this->name = (char) 0;
    this->netId = 0;
    
}

Bank::Bank( int _id, const char* _name ) 
{    
    this->id = _id;
    strcpy( this->name, _name );
    this->netId = 0;   
}

int Bank::GetId()
{
    return this->id;
}


int* Bank::GetIdForMsg()
{
    return &this->id;
}

void Bank::SetNetId( int _netId )
{
    this->netId = _netId;
}

int Bank::getNetId()
{
    return this->netId;
}

char* Bank::GetName()
{
    return this->name;
}

void Bank::SetAddress( const char* _address )
{
    strcpy( this->address, _address );
}

char* Bank::GetAddress()
{
    return this->address;
}

void Bank::SetFullAddress( const char* _address, int _port )
{
    this->SetAddress( _address );
    this->SetNetId( _port );
    
    char fullAddress[30];
    char netId[10];
    
    std::sprintf( netId, ":%d", this->netId );
    
    strcpy( fullAddress, this->address );
    
    strcat( fullAddress, netId);
    
    strcpy( this->fullAddress, fullAddress );
}

void Bank::SetFullAddress( const char* _fullAddress )
{
    strcpy( this->fullAddress, _fullAddress );
}

char* Bank::GetFullAddress()
{
    
    return this->fullAddress;
}

Bank::~Bank(){
    
}


