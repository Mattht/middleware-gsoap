/* 
 * File:   BankProcess.h
 * Author: mattht
 *
 */

//gsoap ns1 service name: nmsrv
//gsoap ns1 service style: rpc
//gsoap ns1 service encoding: encoded
//gsoap ns1 service namespace: urn:nmsrv

//gsoap ns2 service name: bksrv
//gsoap ns2 service style: rpc
//gsoap ns2 service encoding: encoded
//gsoap ns2 service namespace: urn:bksrv

//gsoap ns3 service name: accsrv
//gsoap ns3 service style: rpc
//gsoap ns3 service encoding: encoded
//gsoap ns3 service namespace: urn:accsrv


struct ns1__reqbklistResponse { char* bank1Name; char* bank2Name; int accoundId[2]; };
struct ns1__connectbkResponse { char* fullAddress; };
struct ns1__noResponse{};

int ns1__reqbklist( struct ns1__reqbklistResponse &r );

int ns1__connectbk( int bankId, struct ns1__connectbkResponse &r );

int ns1__register( char* address, int bankId, char* bankName, struct ns1__noResponse *out );


struct ns2__loginResponse { char* account1Name; char* account2Name; int accoundId[2]; };

int ns2__login( char* username, char* password, struct ns2__loginResponse &r );

int ns2__enquire( int accountId, int *currentAmount );

int ns2__deposit( int accountId, int amount, int *currentAmount );

int ns2__withdraw( int accountId, int amount, int *currentAmount );

int ns2__transfer( int accountId, int disBankId, int distAccountId, int amount, int *currentAmount );


int ns3__enquire( int *currentAmount );

int ns3__deposit( int amount, int *currentAmount );

int ns3__withdraw( int amount, int *currentAmount );