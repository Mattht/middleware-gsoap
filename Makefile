GSOAP=soapcpp2
SOAPH=stdsoap2.h
SOAPC=stdsoap2.c
SOAPCPP=stdsoap2.cpp
CC=gcc
CPP=g++
LIBS=
COFLAGS=-O2
CWFLAGS=-Wall
CIFLAGS=-I../..
CDEBUG =-DDEBUG
CMFLAGS=
CFLAGS= $(CWFLAGS) $(COFLAGS) $(CIFLAGS) $(CMFLAGS) $(CDEBUG)
all:		client nameServer bankServer accountServer
client:	BankClient.h BankClient.cpp $(SOAPH) $(SOAPCPP)
		$(GSOAP) -c BankClient.h
		$(CPP) $(CFLAGS) -o client BankClient.cpp soapC.c soapClient.c $(SOAPCPP) $(LIBS)
nameServer: NameServer.h NameServerProcess.cpp $(SOAPH) $(SOAPCPP) Bank.cpp
		$(GSOAP) -c NameServer.h
		$(CPP) $(CFLAGS) -o nameServer NameServerProcess.cpp soapC.c soapServer.c $(SOAPCPP) -lm $(LIBS) Bank.cpp
bankServer: BankProcess.h BankProcess.cpp $(SOAPH) $(SOAPCPP) Bank.cpp BankAccount.cpp
		$(GSOAP) -c BankProcess.h
		$(CPP) $(CFLAGS) -o bank BankProcess.cpp soapC.c soapClient.c soapServer.c $(SOAPCPP) -lm $(LIBS) Bank.cpp BankAccount.cpp
accountServer: AccountProcess.h AccountProcess.cpp $(SOAPH) $(SOAPCPP) BankAccount.cpp
		$(GSOAP) -c AccountProcess.h
		$(CPP) $(CFLAGS) -o account AccountProcess.cpp soapC.c soapServer.c $(SOAPCPP) -lm $(LIBS) BankAccount.cpp

clean:
		rm -f client.exe ns1.add.req.xml ns1.add.res.xml ns1.nsmap ns1.wsdl ns1.xsd server.exe soapClientLib.c soapObject.h soapServerLib.c
