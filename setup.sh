#!/bin/sh

bk1address=node03
bk2address=node04

bk1acc1port=9873
bk1acc2port=9874
bk2acc1port=9875
bk2acc2port=9876

./nameServer 9870 & nspid=$!
./bank $bk1address 9871 101 Barclays $bk1acc1port $bk1acc2port & bank1pid=$!
./bank $bk2address 9872 201 Loyds $bk2acc1port $bk2acc2port & bank2pid=$!
./account $bk1address $bk1acc1port 101 Current 1000 & bk1acc1pid=$!
./account $bk1address $bk1acc2port 102 Savings 3400 & bk1acc2pid=$!
./account $bk2address $bk2acc1port 201 Current 2000 & bk2acc1pid=$!
./account $bk2address $bk2acc2port 202 Savings 1500 & bk2acc2pid=$!

read continue

kill $nspid & kill $bank1pid & kill $bank2pid & kill $bk1acc1pid & kill $bk1acc2pid & kill $bk2acc1pid & kill $bk2acc2pid
