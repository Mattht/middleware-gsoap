/* 
 * File:   AccountProcess.h
 * Author: mattht
 *
 */

//gsoap ns3 service name: accsrv
//gsoap ns3 service style: rpc
//gsoap ns3 service encoding: encoded
//gsoap ns3 service namespace: urn:accsrv


int ns3__enquire( int *currentAmount );

int ns3__deposit( int amount, int *currentAmount );

int ns3__withdraw( int amount, int *currentAmount );
