/* 
 * File:   AccountProcess.cpp
 * Author: mattht
 *
 */

#include "accsrv.nsmap" // generated by gSOAP compiler
#include "BankAccount.h"
using namespace std;

BankAccount* bankAccount;

int main(int argc, char **argv)
{ int m, s; /* master and slave sockets */
  struct soap soap;
  soap_init(&soap);
  /* argv[1] contains port number on which server is started */
  m = soap_bind(&soap, "node04", atoi(argv[2]), 100);
  
  cout << atoi(argv[3]) << " - " << argv[4] << endl;

  bankAccount = new BankAccount( atoi(argv[3]), argv[4], atoi(argv[5]) );
  bankAccount->SetNetId( atoi(argv[2]) );
          
  //int none;
  //soap_call_ns1__register(&soap, nameServerProcess, "", atoi(argv[1]), &none );
  
  //bank = new Bank( atoi(argv[2]), argv[3] );
  
  if (m < 0)
    { soap_print_fault(&soap, stdout);
      exit(-1);
    }
  printf("Socket connection successful: master socket = %d\n", m);
  for ( ; ; ) // infinite loop, servicing requests
    { s = soap_accept(&soap);
      printf("Socket connection successful: slave socket = %d\n", s);
      if (s < 0)
      { soap_print_fault(&soap, stdout);
        exit(-1);
      } 
      soap_serve(&soap);
      soap_end(&soap);
    }
  return 0; // never get to here
} 

int ns3__enquire( struct soap *soap, int *currentAmount )
{ 
    
    *currentAmount = bankAccount->GetBalance();
    
    return SOAP_OK; // flag function successful
}

int ns3__deposit( struct soap *soap, int amount, int *currentAmount )
{ 
    
    bankAccount->Deposit( amount );
    
    *currentAmount = bankAccount->GetBalance();
    
    cout << "New balance: " << bankAccount->GetBalance() << " Account: " << bankAccount->GetId() << endl;
    
    return SOAP_OK; // flag function successful
}

int ns3__withdraw( struct soap *soap, int amount, int *currentAmount )
{ 
    
    if( bankAccount->HasSufficientFunds( amount ) )
    {
        bankAccount->Withdraw( amount );
        
        *currentAmount = bankAccount->GetBalance();
    }
    else
    {
        *currentAmount = -1;	// Balance cannot be negative: Treat this as error msg
    }
    
    return SOAP_OK; // flag function successful
}
