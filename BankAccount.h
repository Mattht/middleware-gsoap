/* 
 * File:   BankAccount.h
 * Author: mattht
 *
 */

#ifndef BANKACCOUNT_H
#define	BANKACCOUNT_H

#include <string.h>
#include <cstdio>

class BankAccount {
public:
    BankAccount();
    BankAccount( int _id, const char* _name, int _balance );
    BankAccount(const BankAccount& orig);
    virtual ~BankAccount();
    
    int GetId();
    int* GetIdForMsg();				// Needed for PVM
    int GetNetId();				// PVM task id or gSOAP Port
    void SetNetId( int _netId );
    void SetAddress( const char* _address );
    char* GetAddress();
    void SetFullAddress( const char* _fullAddress, int _port );	// For gSOAP only
    void SetFullAddress( const char* _fullAddress );
    char* GetFullAddress();
    char* GetName();
    int GetBalance();
    int* GetBalanceForMsg();
    void Deposit( int _ammount );
    bool HasSufficientFunds( int _ammount );
    void Withdraw( int _ammount );
    
private:
    int id, netId, balance;
    char name[10], address[30], fullAddress[30];
};

#endif	/* BANKACCOUNT_H */

