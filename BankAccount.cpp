/* 
 * File:   BankAccount.cpp
 * Author: mattht
 * 
 */

#include "BankAccount.h"

BankAccount::BankAccount() 
{    
    this->id = 0;
    this->netId = 0;
    
}

BankAccount::BankAccount( int _id, const char* _name, int _balance ) 
{    
    this->id = _id;
    strcpy( this->name, _name );
    this->balance = _balance;
    this->netId = 0;
}

BankAccount::BankAccount(const BankAccount& orig) {
}

int BankAccount::GetId()
{
    return this->id;
}

int* BankAccount::GetIdForMsg()
{
    return &this->id;
}

int BankAccount::GetNetId()
{
    return this->netId;
}

void BankAccount::SetNetId( int _netId )
{
    this->netId = _netId;
}

void BankAccount::SetAddress( const char* _address )
{
    strcpy( this->address, _address );
}

char* BankAccount::GetAddress()
{
    return this->address;
}

void BankAccount::SetFullAddress( const char* _address, int _port )
{
    this->SetAddress( _address );
    this->SetNetId( _port );
    
    char fullAddress[30];
    char netId[10];
    
    std::sprintf( netId, ":%d", this->netId );
    
    strcpy( fullAddress, this->address );
    
    strcat( fullAddress, netId);
    
    strcpy( this->fullAddress, fullAddress );
}

void BankAccount::SetFullAddress( const char* _fullAddress )
{
    strcpy( this->fullAddress, _fullAddress );
}

char* BankAccount::GetFullAddress()
{
    
    return this->fullAddress;
}

char* BankAccount::GetName()
{
    return this->name;
}

int BankAccount::GetBalance()
{
    return this->balance;
}

int* BankAccount::GetBalanceForMsg()
{
    return &this->balance;
}

void BankAccount::Deposit( int _ammount )
{
    this->balance += _ammount;
}

bool BankAccount::HasSufficientFunds( int _ammount )
{
    return ( ( this->balance - _ammount ) >= 0 );
}
    
void BankAccount::Withdraw( int _ammount )
{
    this->balance -= _ammount;
}

BankAccount::~BankAccount(){
    
}
